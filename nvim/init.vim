" Stolen 

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Vundle For Managing Plugins
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

set nocompatible              " be iMproved, required
set termguicolors
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim

call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'tpope/vim-fugitive'
Plugin 'neovim/nvim-lspconfig'
" inlay hints etc.
Plugin 'nvim-lua/lsp_extensions.nvim'
Plugin 'hrsh7th/nvim-compe'
Plugin 'simrat39/rust-tools.nvim'
Plugin 'jreybert/vimagit'
Plugin 'vimwiki/vimwiki'
Plugin 'ap/vim-css-color'
Plugin 'styled-components/vim-styled-components', { 'branch': 'main' }
Plugin 'nvim-lua/popup.nvim'
Plugin 'nvim-lua/plenary.nvim'
Plugin 'nvim-telescope/telescope.nvim'
Plugin 'nvim-telescope/telescope-fzy-native.nvim'
Plugin 'airblade/vim-rooter'
Plugin 'prettier/vim-prettier', {
  \ 'do': 'npm install',
  \ 'for': ['javascript', 'typescript', 'css', 'less', 'scss', 'json', 'graphql', 'markdown', 'vue', 'yaml', 'html'] }
Plugin 'ThePrimeagen/vim-be-good'
Plugin 'Yggdroot/indentLine'
Plugin 'psf/black'
Plugin 'dense-analysis/ale'
Plugin 'puremourning/vimspector'
Plugin 'szw/vim-maximizer'
Plugin 'ghifarit53/tokyonight-vim'
Plugin 'farmergreg/vim-lastplace'
Plugin 'vim-airline/vim-airline'
Plugin 'rhysd/git-messenger.vim'
Plugin 'tribela/vim-transparent'
Plugin 'lewis6991/gitsigns.nvim'
" Plugin 'takac/vim-hardtime'  " to stop using jjjj, kkkk to move around
Plugin 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plugin 'nvim-treesitter/nvim-treesitter-angular'
Plugin 'voldikss/vim-floaterm'
Plugin 'cocopon/iceberg.vim'
Plugin 'danilamihailov/beacon.nvim'
Plugin 'folke/todo-comments.nvim'
Plugin 'jose-elias-alvarez/null-ls.nvim'
Plugin 'jose-elias-alvarez/nvim-lsp-ts-utils' " formatting etc. for typescript
Plugin 'norcalli/snippets.nvim'

call vundle#end()

filetype plugin indent on
" To ignore plugin indent changes, instead use:
"filetype plugin on

" show line numbers in netrw (the default file explorer of vim)
let g:netrw_bufsettings = 'noma nomod nu nobl nowrap ro'
let g:netrw_banner = 0
" change the command of netrw to also delete dirs that contain files
let g:netrw_localrmdir = 'rm -r'

" TextEdit might fail if hidden is not set
set hidden

" Some servers have issues with backup files
set nobackup
set nowritebackup

" Give more space for displaying messages
set cmdheight=2

" Having longer updatetime (default is 4000 ms = 4s) leads to noticable
" delays and poor user experience
set updatetime=300

" Don't pass messages to |ins-completion-menu|
set shortmess+=c

" enable project local .nvimrc files
set exrc

" change the minimal jump to show the beacon
let g:beacon_minimal_jump = 1

" make Y behave like C and D i.e. yank from cursor to line end
nnoremap Y y$

" keep the cursor centered (on the monitor) when jumping between search results
nnoremap n nzzzv
nnoremap N Nzzzv

" keep the cursor centered (on the monitor) when concatenating lines
nnoremap J mzJ`z

" automatically place undo breakpoint at certain chars
inoremap , ,<c-g>u
inoremap . .<c-g>u
inoremap ! !<c-g>u
inoremap ? ?<c-g>u
inoremap : :<c-g>u

" move lines around without cluttering the registry
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv
inoremap <C-j> :m .+1<CR>==
inoremap <C-k> :m .-2<CR>==
nnoremap <leader>j :m .+1<CR>==
nnoremap <leader>k :m .-2<CR>==

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Remap Keys
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Disable arrow keys in Normal mode
no <Up> <Nop>
no <Down> <Nop>
no <Left> <Nop>
no <Right> <Nop>

" Disable arrow keys in Insert mode
ino <Up> <Nop>
ino <Down> <Nop>
ino <Left> <Nop>
ino <Right> <Nop>

" set the leader key to space
let mapleader = " "

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Powerline
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Powerline
set rtp+=/usr/share/powerline/bindings/vim/

" Always show statusline
set laststatus=2

" Use 256 colors
set t_Co=256

syntax on
set relativenumber
set number
let g:rehash256 = 1
let g:Powerline_symbols='unicode'
let g:Powerline_theme='long'

let g:airline#extensions#tabline#enabled = 0
let g:airline_powerline_fonts = 0
"let g:airline_theme='angr'

set noshowmode

" show max line length column
set colorcolumn=80

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Text, tab and indent related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Use spaces instead of tabs
set expandtab

" Be smart when using tabs ;)
set smarttab

" 1 tab == 4 spaces
set shiftwidth=4
set tabstop=4
" set the text width to a high number so that vim doesn't break the line
" automatically when it's too long
set tw=999


autocmd Filetype typescript setlocal ts=2 sw=2 sts=0 expandtab
autocmd Filetype javascript setlocal ts=2 sw=2 sts=0 expandtab

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set background=dark
let g:tokyonight_style = 'night'
let g:tokyonight_enable_italic = 1
colorscheme tokyonight
hi LineNr ctermfg=242
hi CursorLineNr ctermfg=15
hi VertSplit ctermfg=8 ctermbg=0
hi Statement ctermfg=3

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Mouse Scrolling
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set mouse=nicr

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Splits and Tabbed Files
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set splitbelow splitright

set path+=**					" Searches current directory recursively.
set wildmenu					" Display all matches when tab complete.
set incsearch
set nobackup
set noswapfile

let g:minimap_highlight='Visual'

let g:python_highlight_all = 1
syntax on

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Custom key mappings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
map <leader><leader> :bn<CR>
map == :bp<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Different indentation per filetype
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

autocmd FileType javascript setlocal shiftwidth=2 tabstop=2
autocmd FileType typescript setlocal shiftwidth=2 tabstop=2
autocmd FileType python setlocal shiftwidth=4 tabstop=4

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Prettier
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

map <C-p> :PrettierAsync<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => LSP
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" configure autocompletion
set completeopt=menuone,noselect

inoremap <silent><expr> <C-Space> compe#complete()
inoremap <silent><expr> <CR>      compe#confirm('<CR>')
inoremap <silent><expr> <C-e>     compe#close('<C-e>')
inoremap <silent><expr> <C-f>     compe#scroll({ 'delta': +4 })
inoremap <silent><expr> <C-d>     compe#scroll({ 'delta': -4 })

lua require("lsp-config")

" Enable type inlay hints for rust
autocmd CursorMoved,InsertLeave,BufEnter,BufWinEnter,TabEnter,BufWritePost *.rs
\ lua require'lsp_extensions'.inlay_hints{ prefix = '', highlight = "Comment", enabled = {"TypeHint", "ChainingHint", "ParameterHint"} }

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Fugitive
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" use vertical splits for the diff view
set diffopt+=vertical

nmap <leader>ga :diffget //3<CR>
nmap <leader>gt :diffget //2<CR>
nmap <leader>gs :G<CR>


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Vimspector
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

fun! GotoWindow(id)
    call win_gotoid(a:id)
    MaximizerToggle
endfun

" start the debug session
nnoremap <leader>dd :call vimspector#Launch()<CR>
nnoremap <leader>dc :call GotoWindow(g:vimspector_session_windows.code)<CR>
nnoremap <leader>dt :call GotoWindow(g:vimspector_session_windows.tagpage)<CR>
nnoremap <leader>dv :call GotoWindow(g:vimspector_session_windows.variables)<CR>
nnoremap <leader>dw :call GotoWindow(g:vimspector_session_windows.watches)<CR>
nnoremap <leader>ds :call GotoWindow(g:vimspector_session_windows.stack_trace)<CR>
nnoremap <leader>do :call GotoWindow(g:vimspector_session_windows.output)<CR>
" exit a debug session
nnoremap <leader>de :call vimspector#Reset()<CR>

nnoremap <leader>dtcb :call vimspector#ClearLineBreakpoint(buffer_name(), line("."))<CR>

nmap <leader>dl <Plug>VimspectorStepInto
nmap <leader>dj <Plug>VimspectorStepOver
nmap <leader>dk <Plug>VimspectorStepOut
nmap <leader>d_ <Plug>VimspectorRestart
nnoremap <leader>d<space> :call vimspector#Continue()<CR>

nmap <leader>drc <Plug>VimspectorRunToCursor
nmap <leader>dbp <Plug>VimspectorToggleBreakpoint
nmap <leader>dcbp <Plug>VimspectorToggleConditionalBreakpoint

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Telescope
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers show_all_buffers=true<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>
" nnoremap <leader>ps :lua require('telescope.builtin').grep_string({ search = vim.fn.input("Grep For > ")})<CR>
" nnoremap <C-p> :lua require('telescope.builtin').git_files()<CR>
" nnoremap <Leader>pf :lua require('telescope.builtin').find_files()<CR>
"
" nnoremap <leader>pw :lua require('telescope.builtin').grep_string { search = vim.fn.expand("<cword>") }<CR>
" nnoremap <leader>pb :lua require('telescope.builtin').buffers()<CR>
" nnoremap <leader>vh :lua require('telescope.builtin').help_tags()<CR>

lua << EOF
local actions = require('telescope.actions')
require('telescope').setup {
    defaults = {
        file_sorter = require('telescope.sorters').get_fzy_sorter,
        prompt_prefix = ' >',
        color_devicons = true,
        file_ignore_patterns = { "node_modules", "venv", "__pycache__", "target" },

        file_previewer   = require('telescope.previewers').vim_buffer_cat.new,
        grep_previewer   = require('telescope.previewers').vim_buffer_vimgrep.new,
        qflist_previewer = require('telescope.previewers').vim_buffer_qflist.new,

        mappings = {
            i = {
                ["<C-x>"] = false,
                ["<C-q>"] = actions.send_to_qflist,
            },
        }
    },
    extensions = {
        fzy_native = {
            override_generic_sorter = false,
            override_file_sorter = true,
        }
    }
}

require('telescope').load_extension('fzy_native')
EOF

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Gitsigns
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

lua << EOF
require('gitsigns').setup()
EOF

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Gitsigns
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

lua <<EOF
require'nvim-treesitter.configs'.setup {
    ensure_installed = {
      'bash',
      'c',
      'css',
      'cpp',
      'dockerfile',
      'dart',
      'go',
      'gomod',
      'graphql',
      'html',
      'java',
      'javascript',
      'jsdoc',
      'json',
      'lua',
      'python',
      'rust',
      'regex',
      'tsx',
      'typescript',
      'toml',
      'yaml',
    },
    highlight = {
      enable = true,
    },
}
EOF

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Floaterm
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

nnoremap <leader>tt :FloatermNew --autoclose=1 --height=0.8 --width=0.8<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Other
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" enable hardtime
let g:hardtime_default_on = 1

" maximize the currently active split
nnoremap <leader>m :MaximizerToggle!<CR>
" auto format python code with black
" autocmd BufWritePre *.py execute ':Black'

" enable auto sort imports on write
" let g:import_sort_auto = 1

" trim whitespace on save
fun! TrimWhitespace()
    let l:save = winsaveview()
    keeppatterns %s/\s\+$//e
    call winrestview(l:save)
endfun

autocmd BufWritePre * :call TrimWhitespace()

" JSONC highlighting

autocmd FileType json syntax match Comment +\/\/.\+$+

" enable rustfmt on save
let g:rustfmt_autosave = 1

" ale
let b:ale_linters = ['pylint']
let g:ale_completion_enabled = 0
let g:ale_open_list = 0
let g:ale_set_loclist = 0
let g:ale_keep_list_window_open = 0

" don't run the local vimrc in a sandbox so that we can use autocmd in it
let g:localvimrc_sandbox = 0
" don't ask before loading a local vimrc and simply load it
let g:localvimrc_ask = 0

" todo highlight
lua << EOF
require("todo-comments").setup {
    highlight = {
        keyword = "bg",
        after = "",
        pattern = [[.*<(KEYWORDS).*:]]
    }
}
EOF
