#!/bin/sh

echo "Installing Typescript LS"
npm i -g typescript typescript-language-server

echo "Installing Eslint"
npm i -g eslint_d

echo "Installing Angular LS"
npm i -g @angular/language-server

echo "Installing CSS + JSON + HTML LS"
npm i -g vscode-langservers-extracted

echo "Installing Dockerfile LS"
npm i -g dockerfile-language-server-nodejs

echo "Installing Prettier"
npm i -g prettier @prettier/plugin-xml
