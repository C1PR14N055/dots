#!/bin/sh

echo "Installing Neovim Bindings"
pip install neovim

echo "Installing Python LS"
pip install jedi-language-server

echo "Installing Pylint"
pip install pylint

echo "Installing Black"
pip install black
